class ChainTest:
    def __init__(self, name: str):
        self.name: str = name

    def a(self):
        self.name = self.name.lower()
        return self

    def b(self):
        print("Name is " + self.name)
        return self

    def c(self, opt: str):
        self.name = f"{self.name} {opt}"
        return self


if __name__ == "__main__":
    (ChainTest('Ivan')
     .a()
     .b()
     .c("is cool")
     .b()
    )
