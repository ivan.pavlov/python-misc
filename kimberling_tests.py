from kimberling import KimProcessor

#k = KimProcessor(num="test", keyword="Barycentrics", debug=True)

#k.main()



line = '[a<sup>12</sup> - 3a<sup>10</sup>(b<sup>2</sup> + c<sup>2</sup>) + a<sup>8</sup>(3b<sup>4</sup> + 7b<sup>2</sup>c<sup>2</sup> + 3c<sup>4</sup>) - 2a<sup>6</sup>(b<sup>2</sup> + c<sup>2</sup>)(b<sup>4</sup> + c<sup>4</sup>)  + 3a<sup>4</sup>(b<sup>2</sup> - c<sup>2</sup>)<sup>2</sup>(b<sup>4</sup> + c<sup>4</sup>) - a<sup>2</sup>(b<sup>2</sup> - c<sup>2</sup>)<sup>2</sup>(3b<sup>6</sup> + b<sup>4</sup>c<sup>2</sup> + b<sup>2</sup>c<sup>4</sup> + 3c<sup>6</sup>) + (b<sup>2</sup> - c<sup>2</sup>)<sup>4</sup>(b<sup>4</sup> + 3b<sup>2</sup>c<sup>2</sup> + c<sup>4</sup>)]/(b<sup>2</sup> + c<sup>2</sup> - a<sup>2</sup>)'
print(
    KimProcessor.tril_process(
        KimProcessor.line_replacer(line)
    )
)