import re
import html


basepath = (
    "C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\Kimberling\\etc\\01000"
)

rdf = "@prefix : <http://etc/> .\n"

for i in range(1, 1000):
    path = f"{basepath}\\X{i}.txt"
    p = open(path, "r", encoding="utf-8", errors="ignore")
    start_now = False

    while True:
        line = html.unescape(p.readline())

        if not line:
            break
        line = line.lstrip()
        # find point name
        if line.startswith("<h3"):
            start = line.find('"')
            end = line.find('"', start + 1)
            point_idx = "X(" + line[start + 2 : end] + ")"
            continue
        
        if line.startswith(point_idx + " lies "):
            start_now = True

        # properties
        if start_now and line.startswith(point_idx + " = "):
            # find all mentioned X()
            points = [
                x.group()
                for x in re.finditer(f"X{re.escape('(')}[0-9]*{re.escape(')')}", line)
            ]
            if len(points) == 2:
                rel = line[1+line.find("= ") : line.find("X(",5)].strip().replace(" ","-")
                rel = re.sub(r"[^A-Za-z0-9" +re.escape("-")+"]", '',rel)
                
                if len(rel)<5 or line.find("triangle")>=0 or line.find("ABC")>=0 or line.find("PU")>=0:
                    continue
                try:
                    points.remove(point_idx)
                except:
                    print(line)
                    raise

                for pt in points:
                    rdf += (
                        (f":{point_idx} :{rel} :{pt} .\n")
                        .replace("(", "")
                        .replace(")", "")
                    )
    p.close()

fout = "C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\pts.rdf"
with open(fout, 'w', encoding="utf-8") as out:
    out.write(rdf)

