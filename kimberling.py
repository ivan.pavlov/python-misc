import pickle


class KimProcessor:

    def __init__(self, keyword="", debug=False) -> None:
        self.keyword = keyword

        self.INPUT = f"C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\kimlast.txt"
        self.INTERIM = 'C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\pointstmp.xml'
        self.OUTPUT = f"C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\pointslast.txt"
        # NAMES = 'C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\point_names_7000.xml'
        self.TRILS = f"C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\tril_last"

        self.DEBUG = debug


    def process_main_file(self):
        p = open(self.INPUT, 'r', encoding="utf-8", errors="ignore")
        file1 = open(self.INTERIM, 'w', encoding="utf-8", errors="ignore")

        while True:

            # Get next line from file
            line = p.readline()

            if not line:
                break

            line = line.lstrip()
            if ( line.startswith("<h3") or line.startswith(self.keyword) ):
                file1.write(self.process_line(line))

        p.close()
        file1.close()

    def process_line(self, line):
        line = line.replace("&nbsp;", " ")
        line = line.replace("&nbsp ;", " ")
        if self.DEBUG:
            print(line)
        try:
            idx = line.index("f(a,b,c) =")
            if self.DEBUG:
                print(idx)
        except:
            try:
                idx = line.index("f(A,B,C) =")
            except:
                try:
                    idx = line.index("g(a,b,c) =")
                except:
                    try:
                        idx = line.index("g(A,B,C) =")
                    except:
                        idx = -1

        if idx >= 0:
            # prepend the keyword as it will be stripped later
            line = self.keyword + " " + line[idx+10:]

        return line

    def process2(self):
        p = open(self.INTERIM, 'r', encoding="utf-8", errors="ignore")

        #file_trils = open(self.OUTPUT, 'w')
        # file_names = open(self.NAMES, 'w')
        file_trils_dict = {}

        trilinears = []
        while True:

            # Get next line from file
            line = p.readline()

            if not line:
                break

            line = line.lstrip()
            if line.startswith("<h3"):
                #process previous point
                if len(trilinears)>0:
                    #find the most appropriate trilinear
                    tril = self.point_process(trilinears)
                    tril = KimProcessor.tril_process(tril)
                    if self.keyword == "Trilinears":
                        #file_trils.write(f'"{point_idx}"->' + '{' + tril.strip() + "},\n")
                        file_trils_dict[point_idx]='a*(' + tril.strip() + ')'
                    if self.keyword == "Barycentrics":
                        #file_trils.write(f'"{point_idx}"->' + '{b*c*(' + tril.strip() + ")},\n")
                        file_trils_dict[point_idx]=tril.strip()
                else:
                    try: #needed for the first line
                        #file_trils.write(f'"{point_idx}"' + "->{},\n")
                        file_trils_dict[point_idx]='{}'
                    except UnboundLocalError:
                        pass
                # try: #needed for the first line
                #     file_names.write(point_name.strip()+"\n")
                # except UnboundLocalError:
                #     pass

                start = line.find('"')
                end = line.find('"', start+1)
                point_idx = line[start+1:end]

                start = line.find('>')
                end = line.find('<',start+1)
                # point_name = line[start+1:end]

                trilinears = []

            if line.startswith(self.keyword):
                line = line.replace(self.keyword,"")
                line = KimProcessor.line_replacer(line)
                if self.DEBUG:
                    print(line)                
                if not any(item in line for item in ['u', 'v', 'w', 'g', 'f', 'x']):
                    trilinears.append(line)

        p.close()
        #file_trils.close()
        #file_names.close()

        with open(f"{self.TRILS}.{self.keyword}", "wb") as trilf:
            pickle.dump(file_trils_dict, trilf)

    @staticmethod
    def line_replacer(line):
        for x in range(1,11,1):
            line = line.replace(f"<sup>{x}</sup>",f"^{x} ")
            line = line.replace(f"<sup>-{x}</sup>",f"^-{x} ")
        line = line.replace("<sup>1/2</sup>","^(1/2)")
        line = line.replace("::",":")
        line = line.replace("<br>","")
        line = line.split(":")[0]
        line = line.replace('[','(').replace(']',')')
        line = line.replace("&pi;", "Pi")
        line = line.replace("&pi ;", "Pi")
        line = line.replace("S<sub>&omega;</sub>", "SW")
        line = line.replace("<sub>","")
        line = line.replace("</sub>"," ")

        return line

    def point_process(self, trilinears)->str:
        # if only one variant use it
        if len(trilinears) == 1:
            return trilinears[0]

        for t in trilinears:
            list1=[]
            list1[:0]=t
            set1 = set(list1)
            for x in range(0,9,1):
                set1.discard(str(x))

            for x in [' ', '^', '/', '+', '-', '*', '(', ')', '[', ']']:
                set1.discard(str(x))
            for x in range(0,10):
                set1.discard(str(x))                
            if set1.issubset({'a','b','c','S','SA','SB','SC'}):
                return t
            else:
                print(t)

        return min(trilinears,key=len)

    @staticmethod
    def tril_process(tril)->str:
        #tril = tril.replace("A", "angleA").replace("B", "angleB").replace("C", "angleC")

        for func in ['sin', 'cos', 'tan', 'cot', 'sec', 'csc', 'Sin', 'Cos', 'Tan', 'Cot', 'Sec', 'Csc']:
            cfunc = func.capitalize()

            for angle in ["A", "B", "C", "A/2", "B/2", "C/2", "A/3", "B/3", "C/3"]:
                tril = tril.replace(f"{func} {angle}", f"{cfunc}[angle{angle}]")
                tril = tril.replace(f"{func}{angle}", f"{cfunc}[angle{angle}]")
                tril = tril.replace(f"{func}({angle})", f"{cfunc}[angle{angle}]")

            for angle in ["2A", "2B", "2C", "3A", "3B", "3C", "3A", "3B", "3C"]:
                a = angle[1]
                num = angle[0]
                tril = tril.replace(f"{func} {angle}", f"{cfunc}[{num}angle{a}]")
                tril = tril.replace(f"{func}{angle}", f"{cfunc}[{num}angle{a}]")

            for angle in ["B - C", "B-C"]:
                tril = tril.replace(f"{func} ({angle})", f"{cfunc}[angleB - angleC]")
                tril = tril.replace(f"{func}({angle})", f"{cfunc}[angleB - angleC]")

            for angle in ["A - C", "A-C"]:
                tril = tril.replace(f"{func} ({angle})", f"{cfunc}[angleA - angleC]")
                tril = tril.replace(f"{func}({angle})", f"{cfunc}[angleA - angleC]")

            for angle in ["A - B", "A-B"]:
                tril = tril.replace(f"{func} ({angle})", f"{cfunc}[angleA - angleB]")
                tril = tril.replace(f"{func}({angle})", f"{cfunc}[angleA - angleB]")

        return tril

    def merge_pickles(self):
        trils = pickle.load( open(f"{self.TRILS}.Trilinears", "rb") )
        barys = pickle.load( open(f"{self.TRILS}.Barycentrics", "rb") )
        results = {}
        for pt, coord in barys.items():
            if coord != "{}":
                results[pt] = coord
            else:
                results[pt] = trils[pt]

        with open(self.OUTPUT, 'w', encoding="utf-8") as out:
            for pt, coord in results.items():
                out.write(f'"{pt}"->' + coord + ",\n")

    def main(self):
        self.process_main_file()
        self.process2()

if __name__ == "__main__":
    KimProcessor(keyword="Trilinears").main()
    KimProcessor(keyword="Barycentrics").main()
    KimProcessor().merge_pickles()
