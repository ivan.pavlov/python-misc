import datetime
import numpy as np
import pandas as pd
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from cachier import cachier

@cachier(stale_after=datetime.timedelta(hours=12))
def get_source_data(url: str) -> pd.DataFrame:
    return pd.read_csv(url)

# pylint: disable=line-too-long
CSV = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'

#CSV = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'

df = get_source_data(CSV)

df = df.drop(['Province/State'], axis=1)
country = df[df['Country/Region'] == 'Bulgaria'].T[3:]
country["num"] = country.sum(axis=1)

# time series
y = country[country['num'] > 1000].loc[:, "num"].to_numpy()
# y = country.loc[:, "num"].diff()[1:].to_numpy()
# x = list(range(1,len(y)+1))
print(y)

fitted_model = ExponentialSmoothing(y, trend='mul').fit(
    smoothing_level=0.8,
    optimized=True,
    use_brute=True,
    use_basinhopping=True)

vrounder = np.vectorize(round)
forecast5 = vrounder(fitted_model.forecast(5))

# print(fitted_model.fittedvalues)
# print("\r\n")
print("forecast....................................................")
print(forecast5)
print("\r\n")
print("summary.....................................................")
print(fitted_model.summary())
print("\r\n")
print("mle retvals.................................................")
print(fitted_model.mle_retvals)
