import pickle


class KimProcessorNonETC:

    def __init__(self) -> None:
        self.INPUT = f"C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\nonetc.txt"
        self.OUTPUT = f"C:\\Users\\ivan.pavlov\\Documents\\Wolfram Mathematica\\tmp\\pointslast.txt"


    def process(self, start):
        p = open(self.INPUT, 'r', encoding="utf-8", errors="ignore")

        barys = {}
        oldline = ""
        while True:

            # Get next line from file
            line = p.readline()

            if not line:
                break

            line = line.lstrip()
            if line.startswith("Barycentrics"):
                barys[oldline] = line.replace("Barycentrics","").strip()

            oldline = line

        p.close()

        i = start
        with open(self.OUTPUT, 'w', encoding="utf-8") as out:
            for pt, coord in barys.items():
                out.write(f'"X{i}"->' + coord + ",\n")
                i = i + 1

if __name__ == "__main__":
    KimProcessorNonETC().process(1000000)
