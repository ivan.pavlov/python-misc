def csv_to_fixed_width(local_csv_file: str, local_output_file: str, skip_header=True):
    with open(local_csv_file, "r") as f:
        data = f.readlines()

    if skip_header:
        data.pop(0)

    w = {}
    for line in data:
        for col_nr, col in enumerate(line.strip().split(",")):
            w[col_nr] = max(w.get(col_nr, 0), len(col))

    with open(local_output_file, "w") as f:
        for line in data:
            for col_nr, col in enumerate(line.strip().split(",")):
                # the :<{w[col_nr]+5}} - part is left-adjusting to certain width
                f.write(f"{col:<{w[col_nr]+5}}")  # 5 additional spaces
            f.write("\n")
