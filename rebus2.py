"""
Look for the maximum value over all permutations
"""
import itertools

TARGET = 66
smax = -1

for x in itertools.permutations([1, 2, 3, 4, 5, 6, 7, 8, 9]):
    if x[1]%x[2] + (x[6]*x[7])%x[8] > 0:
        continue

    snew = x[0] + 13*x[1]/x[2] + x[3] + 12*x[4] - x[5] - 11 + x[6]*x[7]/x[8] - 10

    if snew > smax:
        print(x)
        print("s="+str(snew))
        smax = snew
