import os
import pickle
import re as regex
import sys
from uuid import uuid4

from mpmath import *

mp.dps=50

def linecheck(lfile, barys, prec=1e-20, debug=False):
    results = {}
    for ptname in barys:
        results[ptname]=[]
    i = 0;
    with open(lfile) as f:
        for l in f:
            if debug and i%100000==0:
                print(i)
            i = i + 1            
            line = l.strip().split(',')
            mpline = list(map(mpf, line[1:]))
            for ptname in barys:
                check = fdot(barys[ptname], mpline)
                if almosteq(check, 0, abs_eps=prec):
                    results[ptname].append(line)
                    print(f"Found line for {ptname}: {line}!")
    return results

def pointcheck(pfile, barys, prec=1e-20, debug = False):
    """Check if a point coinides with a point from a file

    Args:
        pfile (_type_): input file name
        barys (_type_): points to check, dict passed through str_to_mpf()
    """
    results = {}
    for ptname in barys:
        results[ptname]=[]
    i = 0;
    with open(pfile) as f:
        for l in f:
            if debug and i%100000==0:
                print(i)
            i = i + 1
            pt = l.strip().split(',')
            try:
                mpt = list(map(mpf, pt[1:]))
            except ValueError:
                # complex number
                continue
            for ptname in barys:
                check = (
                    almosteq(barys[ptname][0], mpt[0], abs_eps=prec) &
                    almosteq(barys[ptname][1], mpt[1], abs_eps=prec) &
                    almosteq(barys[ptname][2], mpt[2], abs_eps=prec) 
                )
                if check:
                    results[ptname].append(pt)
                    print(f"Found point {ptname}: {pt[0]}!")
    return results

def str_to_mpf(coords: str):
    coords = coords.split(',')
    mpt = [mpf(coords[1].strip()), mpf(coords[2].strip()), mpf(coords[3].strip())]
    ptnorm = sqrt(power(mpt[0],2) + power(mpt[1],2) + power(mpt[2],2))
    mpt[0] = fdiv(mpt[0], ptnorm)
    mpt[1] = fdiv(mpt[1], ptnorm)
    mpt[2] = fdiv(mpt[2], ptnorm)
    return {coords[0].strip(): mpt}

def mystrip(instr: str):
    return regex.sub(r'(\s|\u180B|\u200B|\u200C|\u200D|\u2060|\uFEFF)+', '', instr)


if __name__ == "__main__":
    if len(sys.argv)>=4:
        precision = pow(10,-int(sys.argv[3]))
    else:
        precision = 20

    if len(sys.argv)>=5:
        debug_mode = sys.argv[4].lower()=='true'
    
    ptlist = {}
    with open(sys.argv[1]) as checkpoints_file:
        for pt in checkpoints_file:
            ptlist |= str_to_mpf(pt)

    all_points = ptlist.copy()

    findings = []
    with open(sys.argv[2]) as coordfilelist:
        for coordfile in coordfilelist:
            if len(ptlist)==0:
                break
            try:
                coordfile = coordfile.strip()
                if not os.path.isfile(coordfile):
                    print(f"Skipping {coordfile}")
                    continue
                print(f"Starting {os.path.basename(coordfile)}")
                new_findings = {}
                if 'outlines' in coordfile:
                    new_findings = linecheck(coordfile, ptlist, precision, debug_mode)
                if 'crossConjugate' in coordfile:
                    new_findings = pointcheck(coordfile, ptlist, precision, debug_mode)
                if 'IsogonalConjugate' in coordfile:
                    new_findings = pointcheck(coordfile, ptlist, precision, debug_mode)                     
                if 'ETCBary' in coordfile:
                    new_findings = pointcheck(coordfile, ptlist, precision, debug_mode)
                findings.append(new_findings)
                # Exit if we found an etc point
                for ptname in all_points:
                    if new_findings.get(ptname):
                        if len(new_findings[ptname])>0 and 'ETCBary' in coordfile:
                            print(new_findings[ptname])
                            ptlist.pop(ptname)
            except KeyboardInterrupt:
                break

    findings_final = {}
    for ptname in all_points: #aggregate findings across files
        findings_final[ptname]=[]
        for fff in findings:
            findings_final[ptname] += fff.get(ptname, [])


    print(findings_final)

    findings_file_name = str(uuid4())

    with open(findings_file_name, 'wb') as ff:
        pickle.dump(findings_final, ff)
