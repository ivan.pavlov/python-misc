import os
import sys
import xml.etree.ElementTree as ET
from pyspark.sql.session import SparkSession


def parse_xml(root, structure: dict, main_tag) -> list:
    """Extractsn the information specified in structure dict about
    all instaces of main_tag. It will not recurse into nested tags.
    Returns
    -------
    list
        List of copies of the stucture dict
    """
    data = []

    for item in root.findall(main_tag):
        maindata = structure.copy()

        for child in item:
            if child.text is not None:
                # build a unique data indentifier
                if child.tag in maindata:
                    maindata[child.tag] = child.text.strip()

        data.append(maindata)

    return data

def parse_xml_linked(root, structure: dict, main_tag, link_tag="", link_attrib_id="") -> list:
    """Extracts the information specified in the structure dict
    about all instaces of link_tag.. It is expected that link_tag is child of main_tag.
    Adds link_attrib_id value from main_tag.
    Returns
    -------
    list
        List of copies of the stucture dict
    """
    data = []

    for item in root.findall(main_tag):
        for child in item.findall(link_tag):
            maindata = structure.copy()
            # create new field name for the output dict
            # to indicate parent tag name and avoid collisions
            link_attrib_tag = f"{item.tag}.{link_attrib_id}"
            maindata[link_attrib_tag] = item.attrib[link_attrib_id]

            for a in child:
                if a.text is not None:
                    if a.tag in maindata:
                        maindata[a.tag] = a.text.strip()

            data.append(maindata)

    return data


# WHAT FOLLOWS IS AN EXAMPLE OF USE

invoice_structure = {
    "InvoiceNumber":"",
    "CustomerOrderNumber":"",
    "AccountReference":"",
    "InvoiceDate":"",
    "ItemsNet":"",
    "ItemsTax":"",
    "ItemsTotal":"",
    "AmountPaid":""
}

item_structure = {
    "Sku":"",
    "Name":""
}

example_tree = ET.parse("xml_to_parquet.xml")

example_root = example_tree.getroot()

tag = './Invoices/Invoice'

# res = parse_xml(example_root, invoice_structure, tag)
# print(res)

res = parse_xml_linked(example_root, item_structure, tag, 'InvoiceItems/Item', 'id')
#print(res)

# CONFIGURE SPARK
os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

spark = (SparkSession
        .builder
        .appName("test")
        .config("spark.ui.port", "12344")
        .getOrCreate()
        )

# convert to df
df = spark.createDataFrame(res)
df.show()
