import sys

class Polyomino():

    def __init__(self, iterable):
        self.squares = tuple(sorted(iterable))

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, repr(self.squares))

    def __iter__(self):
        return iter(self.squares)

    def __len__(self):
        return len(self.squares)

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __hash__(self):
        """Determine the one-sided key for this Poylomino"""
        poly = self.translate()
        k = poly.key()
        for _ in range(3):
            poly = poly.rotate().translate()
            k = min(k, poly.key())
        return k

    def key(self):
        return hash(self.squares)

    def rotate(self):
        """Return a Polyomino rotated clockwise"""
        return Polyomino((-y, x) for x, y in self)

    def translate(self):
        """Return a Polyomino Translated to 0,0"""
        min_x = min(s[0] for s in self)
        min_y = min(s[1] for s in self)
        return Polyomino((x-min_x, y-min_y) for x, y in self)

    def raise_order(self):
        """Return a list of higher order Polyonominos evolved from self"""
        polyominoes = []
        for square in self:
            adjacents = (adjacent for adjacent in (
                (square[0] + 1, square[1]),
                (square[0] - 1, square[1]),
                (square[0], square[1] + 1),
                (square[0], square[1] - 1),
            ) if adjacent not in self)
            for adjacent in adjacents:
                polyominoes.append(
                    Polyomino(list(self) + [adjacent])
                )
        return polyominoes

    def render(self):
        """
        Returns a string map representation of the Polyomino
        """
        poly = self.translate()
        order = len(poly)
        return ''.join(
            ["\n %s" % (''.join(
                ["□" if (x, y) in poly.squares else "-" for x in range(order)]
            )) for y in range(order)]
        )


def count_polys(target: int):
    order = 1
    polyominoes = set([Polyomino(((0, 0),))])

    while order < target:
        order += 1
        next_order_polyominoes = set()
        for polyomino in polyominoes:
            next_order_polyominoes.update(polyomino.raise_order())
        polyominoes = next_order_polyominoes

    return polyominoes

if __name__ == "__main__":
    polys = count_polys(int(sys.argv[1]))
    print(len(polys))
    for pi in polys:
        print(pi.render())
