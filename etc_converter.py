import sys


def check_conditions(conditions, nlin, ncon, noth):
    return (
        nlin >= conditions['lines'] and 
        ncon + nlin >= conditions['lines + conics'] and
        noth >= conditions['others']
    )

inputfile = sys.argv[1]
outputfile = sys.argv[1]+".etc.html"

heading_tag = 'Q(X('

conditions1 = {
    'lines': 2,
    'lines + conics': 2,
    'others': 0
}

conditions2 = {
    'lines': 2,
    'lines + conics': 0,
    'others': 1
}

ifile = open(inputfile, 'r', encoding="utf-8", errors="ignore")
allines = ifile.read().splitlines() # List with stripped line-breaks
ifile.close() # Close file

nx = 60000
pts = {}
pts[nx] = []

for line in allines:
    if (heading_tag in line and "=" not in line) or "===" in line or "---" in line:
        nx = nx + 1
        pts[nx] = []
    if len(line.strip())>0:
        pts[nx].append(line)

ofile = open(outputfile, 'w', encoding="utf-8")

names = []
duplicates = []

for ptn, linearr in pts.items():

    numlines = 0
    numconics = 0
    numothers = 0

    entry = ''
    
    start_looking_for_name = True

    for line in linearr:
        if "linear combinations: " in line:
            continue

        outline = line

        if start_looking_for_name:
            if "X(" in line or line.startswith("NAME: "):
                ptname = 'X('+str(ptn)+')'
                line = line.replace("NAME: ", "").replace("∩","&cap;").strip()
                outline = '<h3 id="X'+str(ptn)+'">'+ptname+' = '+line+'</h3>\n'

                if line.strip() in names:
                    duplicates.append(line)
                    break
                
                start_looking_for_name = False
                names.append(line.strip())
            else:
                continue
        
        if "Barycentrics " in line:
            outline = 'Barycentrics &nbsp;&nbsp; '+ line.replace('Barycentrics','').strip() +' : : \n'
            checklin = [x for x in linearr if "linear combinations: " in x]
            if len(checklin)>0:
                outline += '<b><br>\n'
                outline += ptname + ' = '+ checklin[0].replace('linear combinations:','').strip() 
                outline += '\n</b>'
            outline += '<p>\nplaceholder\n</p><p>\n'

        if "lies on" in line and "lines" in line:
            outline = ptname + ' ' + line.strip()
            outline += '\n</p><p>\n'
            numlines = line.count('{') # number of detected lines

        if line.strip().startswith('='):
            #detect some empty lines
            if ':' in line and not '{' in line.split(':')[1]:
                continue
            outline = ptname + ' ' + line.strip() + "<br>\n"
            if "X(" not in line and 'pole of line {' not in line and 'circumconic {{' not in line:
                print ("Possible empty line")
                print (outline)
                continue
            if 'intersection' in line and 'circumconics' in line:
                numconics = line.count('{{') # number of detected circumconics
            else:
                numothers = numothers + 1

        if 'curves' in line:
            print (outline)

        entry = entry + outline
        
    if (
        (
            check_conditions(conditions1, numlines, numconics, numothers) or
            check_conditions(conditions2, numlines, numconics, numothers)
        )
        and
        len(entry)>0
    ):
        ofile.write(entry)
        ofile.write('</p><hr class="gray">\n')
    else:
        print(ptname)
        print(len(entry))
        print([conditions1, numlines, numconics, numothers])

ofile.close()

print(duplicates)
